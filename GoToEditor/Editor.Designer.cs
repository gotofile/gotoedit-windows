﻿namespace GoToEditor
{
    partial class Editor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Editor));
            this.mainMenuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.urlLabel = new System.Windows.Forms.Label();
            this.urlTextBox = new System.Windows.Forms.TextBox();
            this.textBoxContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.copyContextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteContextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectAllContextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.urlOpenLabel = new System.Windows.Forms.LinkLabel();
            this.linkContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.copyURLContextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notesLabel = new System.Windows.Forms.Label();
            this.notesTextBox = new System.Windows.Forms.TextBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.mainMenuStrip.SuspendLayout();
            this.textBoxContextMenuStrip.SuspendLayout();
            this.linkContextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainMenuStrip
            // 
            this.mainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem});
            resources.ApplyResources(this.mainMenuStrip, "mainMenuStrip");
            this.mainMenuStrip.Name = "mainMenuStrip";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveasToolStripMenuItem,
            this.toolStripSeparator1,
            this.aboutToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            resources.ApplyResources(this.fileToolStripMenuItem, "fileToolStripMenuItem");
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            resources.ApplyResources(this.newToolStripMenuItem, "newToolStripMenuItem");
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            resources.ApplyResources(this.openToolStripMenuItem, "openToolStripMenuItem");
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            resources.ApplyResources(this.saveToolStripMenuItem, "saveToolStripMenuItem");
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveasToolStripMenuItem
            // 
            this.saveasToolStripMenuItem.Name = "saveasToolStripMenuItem";
            resources.ApplyResources(this.saveasToolStripMenuItem, "saveasToolStripMenuItem");
            this.saveasToolStripMenuItem.Click += new System.EventHandler(this.saveasToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            resources.ApplyResources(this.toolStripSeparator1, "toolStripSeparator1");
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            resources.ApplyResources(this.aboutToolStripMenuItem, "aboutToolStripMenuItem");
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.selectAllToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            resources.ApplyResources(this.editToolStripMenuItem, "editToolStripMenuItem");
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            resources.ApplyResources(this.copyToolStripMenuItem, "copyToolStripMenuItem");
            this.copyToolStripMenuItem.Click += new System.EventHandler(this.copyToolStripMenuItem_Click);
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            resources.ApplyResources(this.pasteToolStripMenuItem, "pasteToolStripMenuItem");
            this.pasteToolStripMenuItem.Click += new System.EventHandler(this.pasteToolStripMenuItem_Click);
            // 
            // selectAllToolStripMenuItem
            // 
            this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
            resources.ApplyResources(this.selectAllToolStripMenuItem, "selectAllToolStripMenuItem");
            this.selectAllToolStripMenuItem.Click += new System.EventHandler(this.selectAllToolStripMenuItem_Click);
            // 
            // urlLabel
            // 
            resources.ApplyResources(this.urlLabel, "urlLabel");
            this.urlLabel.Name = "urlLabel";
            // 
            // urlTextBox
            // 
            resources.ApplyResources(this.urlTextBox, "urlTextBox");
            this.urlTextBox.ContextMenuStrip = this.textBoxContextMenuStrip;
            this.urlTextBox.Name = "urlTextBox";
            this.urlTextBox.TextChanged += new System.EventHandler(this.urlTextBox_TextChanged);
            // 
            // textBoxContextMenuStrip
            // 
            this.textBoxContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyContextToolStripMenuItem,
            this.pasteContextToolStripMenuItem,
            this.selectAllContextToolStripMenuItem});
            this.textBoxContextMenuStrip.Name = "textBoxContextMenuStrip";
            resources.ApplyResources(this.textBoxContextMenuStrip, "textBoxContextMenuStrip");
            // 
            // copyContextToolStripMenuItem
            // 
            this.copyContextToolStripMenuItem.Name = "copyContextToolStripMenuItem";
            resources.ApplyResources(this.copyContextToolStripMenuItem, "copyContextToolStripMenuItem");
            this.copyContextToolStripMenuItem.Click += new System.EventHandler(this.copyContextToolStripMenuItem_Click);
            // 
            // pasteContextToolStripMenuItem
            // 
            this.pasteContextToolStripMenuItem.Name = "pasteContextToolStripMenuItem";
            resources.ApplyResources(this.pasteContextToolStripMenuItem, "pasteContextToolStripMenuItem");
            this.pasteContextToolStripMenuItem.Click += new System.EventHandler(this.pasteContextToolStripMenuItem_Click);
            // 
            // selectAllContextToolStripMenuItem
            // 
            this.selectAllContextToolStripMenuItem.Name = "selectAllContextToolStripMenuItem";
            resources.ApplyResources(this.selectAllContextToolStripMenuItem, "selectAllContextToolStripMenuItem");
            this.selectAllContextToolStripMenuItem.Click += new System.EventHandler(this.selectAllContextToolStripMenuItem_Click);
            // 
            // urlOpenLabel
            // 
            resources.ApplyResources(this.urlOpenLabel, "urlOpenLabel");
            this.urlOpenLabel.ContextMenuStrip = this.linkContextMenuStrip;
            this.urlOpenLabel.Name = "urlOpenLabel";
            this.urlOpenLabel.TabStop = true;
            this.urlOpenLabel.Click += new System.EventHandler(this.urlOpenLabel_Click);
            // 
            // linkContextMenuStrip
            // 
            this.linkContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyURLContextToolStripMenuItem});
            this.linkContextMenuStrip.Name = "linkContextMenuStrip";
            resources.ApplyResources(this.linkContextMenuStrip, "linkContextMenuStrip");
            // 
            // copyURLContextToolStripMenuItem
            // 
            this.copyURLContextToolStripMenuItem.Name = "copyURLContextToolStripMenuItem";
            resources.ApplyResources(this.copyURLContextToolStripMenuItem, "copyURLContextToolStripMenuItem");
            this.copyURLContextToolStripMenuItem.Click += new System.EventHandler(this.copyURLContextToolStripMenuItem_Click);
            // 
            // notesLabel
            // 
            resources.ApplyResources(this.notesLabel, "notesLabel");
            this.notesLabel.Name = "notesLabel";
            // 
            // notesTextBox
            // 
            resources.ApplyResources(this.notesTextBox, "notesTextBox");
            this.notesTextBox.ContextMenuStrip = this.textBoxContextMenuStrip;
            this.notesTextBox.Name = "notesTextBox";
            this.notesTextBox.TextChanged += new System.EventHandler(this.notesTextBox_TextChanged);
            // 
            // openFileDialog
            // 
            this.openFileDialog.DefaultExt = "goto";
            resources.ApplyResources(this.openFileDialog, "openFileDialog");
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.DefaultExt = "goto";
            resources.ApplyResources(this.saveFileDialog, "saveFileDialog");
            // 
            // Editor
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.notesTextBox);
            this.Controls.Add(this.notesLabel);
            this.Controls.Add(this.urlOpenLabel);
            this.Controls.Add(this.urlTextBox);
            this.Controls.Add(this.urlLabel);
            this.Controls.Add(this.mainMenuStrip);
            this.MainMenuStrip = this.mainMenuStrip;
            this.Name = "Editor";
            this.Icon = global::GoToEditor.Properties.Resources.Icon;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Editor_FormClosing);
            this.Load += new System.EventHandler(this.Editor_Load);
            this.mainMenuStrip.ResumeLayout(false);
            this.mainMenuStrip.PerformLayout();
            this.textBoxContextMenuStrip.ResumeLayout(false);
            this.linkContextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mainMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.Label urlLabel;
        private System.Windows.Forms.TextBox urlTextBox;
        private System.Windows.Forms.LinkLabel urlOpenLabel;
        private System.Windows.Forms.Label notesLabel;
        private System.Windows.Forms.TextBox notesTextBox;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip textBoxContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem copyContextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteContextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectAllContextToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip linkContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem copyURLContextToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
    }
}

