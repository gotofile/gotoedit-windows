﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace GoToEditor
{
    public partial class Editor : Form
    {
        GoToFileV1 file;
        bool edited = false;

        public Editor(GoToFileV1 file)
        {
            this.file = file;
            InitializeComponent();
            urlTextBox.Text = file.url;
            if (file.notes == null)
            {
                notesTextBox.Text = "";
            }
            else
            {
                notesTextBox.Text = file.notes.Replace("\n", Environment.NewLine);
            }
            edited = false;
        }

        private void setTitle()
        {
            if (file.FilePath == null)
            {
                Text = Strings.Strings.UntitledFilename;
            }
            else
            {
                Text = file.FilePath.Replace("\\\\", "\\");
            }
            if (edited)
            {
                Text += "*";
            }
            Text += " - " + Application.ProductName;
        }

        private void safeCopy(string text)
        {
            // this function exists because Clipboard.SetText throws an exception if text == ""
            // this function therefore does not attempt to modify the clipboard if text == null || text == ""
            if (text != null && text != "")
            {
                Clipboard.SetText(text);
            }
        }

        private void copyIfFocused(TextBox textBox)
        {
            if (textBox.Focused)
            {
                if (textBox.SelectionLength == 0)
                {
                    safeCopy(textBox.Text);
                }
                else
                {
                    safeCopy(textBox.SelectedText);
                }
            }
        }

        private void copy()
        {
            copyIfFocused(urlTextBox);
            copyIfFocused(notesTextBox);
        }

        private void pasteIfFocused(TextBox textBox)
        {
            if (textBox.Focused)
            {
                textBox.Paste(Clipboard.GetText());
            }
        }

        private void paste()
        {
            pasteIfFocused(urlTextBox);
            pasteIfFocused(notesTextBox);
        }

        private void selectAllIfFocused(TextBox textBox)
        {
            if (textBox.Focused)
            {
                textBox.SelectionStart = 0;
                textBox.SelectionLength = textBox.Text.Length;
            }
        }

        private void selectAll()
        {
            selectAllIfFocused(urlTextBox);
            selectAllIfFocused(notesTextBox);
        }

        private bool save(bool forceChooseLocation)
        {
            if (forceChooseLocation || file.FilePath == null)
            {
                if (file.FilePath == null)
                {
                    saveFileDialog.FileName = Strings.Strings.UntitledFilename;
                }
                else
                {
                    saveFileDialog.FileName = Path.GetFileName(file.FilePath);
                }
                if (saveFileDialog.ShowDialog() != DialogResult.OK)
                {
                    return false;
                }
                file.FilePath = saveFileDialog.FileName;
            }
            try
            {
                File.WriteAllText(file.FilePath, file.Serialize());
            }
            catch (Exception)
            {
                MessageBox.Show(this, Strings.Strings.Error_Save, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            edited = false;
            setTitle();
            return true;
        }

        private void Editor_Load(object sender, EventArgs e)
        {
            setTitle();
        }

        private void Editor_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (edited)
            {
                DialogResult result = MessageBox.Show(this, Strings.Strings.SaveChangesPrompt, Application.ProductName, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button3);
                if (result == DialogResult.Cancel)
                {
                    e.Cancel = true;
                }
                else if (result == DialogResult.Yes)
                {
                    if (!save(false))
                    {
                        e.Cancel = true;
                    }
                }
            }
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start(Application.ExecutablePath, "/new");
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                Process.Start(Application.ExecutablePath, "\"" + openFileDialog.FileName.Replace("\"", "\"\"").Replace("\\", "\\\\") + "\"");
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new AboutBox().ShowDialog(this);
        }

        private void urlTextBox_TextChanged(object sender, EventArgs e)
        {
            edited = true;
            setTitle();
            file.url = urlTextBox.Text;
        }

        private void notesTextBox_TextChanged(object sender, EventArgs e)
        {
            edited = true;
            setTitle();
            if (notesTextBox.Text == "")
            {
                file.notes = null;
            }
            else
            {
                file.notes = notesTextBox.Text.Replace(Environment.NewLine, "\n");
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            save(false);
        }

        private void saveasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            save(true);
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            copy();
        }

        private void copyContextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            copy();
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            paste();
        }

        private void pasteContextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            paste();
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            selectAll();
        }

        private void selectAllContextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            selectAll();
        }

        private void copyURLContextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            safeCopy(file.url);
        }

        private void urlOpenLabel_Click(object sender, EventArgs e)
        {
            try
            {
                file.Open();
            }
            catch (Exception)
            {
                MessageBox.Show(this, Strings.Strings.Error_InvalidURL, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
