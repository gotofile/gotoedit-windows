﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.Json;

namespace GoToEditor
{
    public class GoToFileV1
    {
        static JsonSerializerOptions serializerOptions = new JsonSerializerOptions
        {
            IgnoreNullValues = true
        };

        public int version { get; set; }
        public string url { get; set; }
        public string notes { get; set; }

        public string FilePath;

        public GoToFileV1()
        {
            url = "";
            version = 1;
        }

        public GoToFileV1(string filename)
        {
            version = 1;
            // TODO: read file
            url = "";
            notes = null;
            FilePath = filename;
        }

        public GoToFileV1(string url, string notes)
        {
            version = 1;
            url = "";
            notes = null;
            FilePath = null;
        }

        public void Open()
        {
            Uri uriResult;
            bool isUrlValid = Uri.TryCreate(url, UriKind.Absolute, out uriResult);
            if (isUrlValid && !uriResult.IsFile)
            {
                Process.Start(uriResult.AbsoluteUri);
            }
            else
            {
                throw new Exception("Invalid URL");
            }
        }

        public string Serialize()
        {
            string path = FilePath;
            string serialized = JsonSerializer.Serialize(this, serializerOptions);
            return serialized;
        }

        public static GoToFileV1 Deserialize(string json, string filename = null)
        {
            GoToFileV1 file = JsonSerializer.Deserialize<GoToFileV1>(json, GoToFileV1.serializerOptions);
            if (filename != null)
            {
                file.FilePath = filename;
            }
            if (file.url == null)
            {
                file.url = "";
            }
            if (file.version != 1)
            {
                throw new Exception("Invalid version");
            }
            return file;
        }
    }
}
