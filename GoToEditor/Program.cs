﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Resources;
using System.Windows.Forms;

namespace GoToEditor
{
    static class Program
    {
        private static GoToFileV1 getFile(string filename)
        {
            GoToFileV1 file;
            try
            {
                file = GoToFileV1.Deserialize(File.ReadAllText(filename), filename);
            }
            catch (Exception ex)
            {
                if (ex.Message == "Invalid version")
                {
                    MessageBox.Show(Strings.Strings.Error_Unsupported, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show(Strings.Strings.Error_ReadError, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                throw new Exception("Could not open file", ex);
            }
            return file;
        }

        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            if (args.Length == 0)
            {
                Application.Run(new WelcomeForm());
            }
            else
            {
                string lowercaseArg = args[0].ToLower();
                if (lowercaseArg == "/help" || lowercaseArg == "/?" || lowercaseArg == "/h")
                {
                    MessageBox.Show(
                        "Available options:"
                        + Environment.NewLine + "/help, /h, /?: Display this message"
                        + Environment.NewLine + "<path>: Open the specified file in the editor"
                        + Environment.NewLine + "/about: Open About window"
                        + Environment.NewLine + "/fileregister: Register file handler (requires elevation)"
                        + Environment.NewLine + "/fileunregister: Unregister file handler (requires elevation)"
                        + Environment.NewLine + "/new: Open editor a new file in the editor"
                        + Environment.NewLine + "/view <path>: View a GoTo file (opens the link in the browser or shows notes if present)"
                        + Environment.NewLine + "/viewnotes <path>: View the notes inside a GoTo file"
                        + Environment.NewLine + "/viewurl <path>: Open the URL inside a GoTo file"
                    );
                }
                else if (lowercaseArg == "/about")
                {
                    Application.Run(new AboutBox());
                }
                else if (lowercaseArg == "/fileregister")
                {
                    // Main file definition
                    RegistryKey key = Registry.ClassesRoot.CreateSubKey("GoToFile");
                    key.SetValue("", Strings.Strings.FileTypeName);
                    key.Close();
                    key = Registry.ClassesRoot.CreateSubKey("GoToFile\\Shell");
                    key.SetValue("", "View"); // Default action

                    // File icon
                    key = Registry.ClassesRoot.CreateSubKey("GoToFile\\DefaultIcon");
                    key.SetValue("", "\"" + Path.Combine(
                        Path.GetDirectoryName(Application.ExecutablePath),
                        "GoToEditorWin32Resources.dll"
                    ).Replace("\"", "\"\"").Replace("\\", "\\\\") + "\",1");
                    key.Close();

                    // Edit action
                    key = Registry.ClassesRoot.CreateSubKey("GoToFile\\Shell\\Edit");
                    key.SetValue("", Strings.Strings.FileAssoc_Edit);
                    key.Close();
                    key = Registry.ClassesRoot.CreateSubKey("GoToFile\\Shell\\Edit\\Command");
                    key.SetValue("", "\"" + Application.ExecutablePath.Replace("\"", "\"\"").Replace("\\", "\\\\") + "\" \"%L\"");
                    key.Close();

                    // View action
                    key = Registry.ClassesRoot.CreateSubKey("GoToFile\\Shell\\View");
                    key.SetValue("", Strings.Strings.FileAssoc_View);
                    key.Close();
                    key = Registry.ClassesRoot.CreateSubKey("GoToFile\\Shell\\View\\Command");
                    key.SetValue("", "\"" + Application.ExecutablePath.Replace("\"", "\"\"").Replace("\\", "\\\\") + "\" /view \"%L\"");
                    key.Close();

                    // ViewNotes action
                    key = Registry.ClassesRoot.CreateSubKey("GoToFile\\Shell\\ViewNotes");
                    key.SetValue("", Strings.Strings.FileAssoc_ViewNotes);
                    key.Close();
                    key = Registry.ClassesRoot.CreateSubKey("GoToFile\\Shell\\ViewNotes\\Command");
                    key.SetValue("", "\"" + Application.ExecutablePath.Replace("\"", "\"\"").Replace("\\", "\\\\") + "\" /viewnotes \"%L\"");
                    key.Close();

                    // ViewURL action
                    key = Registry.ClassesRoot.CreateSubKey("GoToFile\\Shell\\ViewURL");
                    key.SetValue("", Strings.Strings.FileAssoc_ViewURL);
                    key.Close();
                    key = Registry.ClassesRoot.CreateSubKey("GoToFile\\Shell\\ViewURL\\Command");
                    key.SetValue("", "\"" + Application.ExecutablePath.Replace("\"", "\"\"").Replace("\\", "\\\\") + "\" /viewurl \"%L\"");
                    key.Close();

                    // .goto association
                    key = Registry.ClassesRoot.CreateSubKey(".goto");
                    key.SetValue("", "GoToFile");
                    key.Close();

                    // Add support for creating files from the "New..." context menu in explorer
                    key = Registry.ClassesRoot.CreateSubKey(".goto\\ShellNew");
                    GoToFileV1 empty = new GoToFileV1("", null);
                    File.WriteAllText("C:\\Windows\\ShellNew\\Empty.goto", empty.Serialize());
                    key.SetValue("FileName", "Empty.goto");
                    key.Close();
                }
                else if (lowercaseArg == "/fileunregister")
                {
                    Registry.ClassesRoot.DeleteSubKeyTree("GoToFile");
                    Registry.ClassesRoot.DeleteSubKeyTree(".goto");
                    File.Delete("C:\\Windows\\ShellNew\\Empty.goto");
                }
                else if (lowercaseArg == "/new")
                {
                    Application.Run(new Editor(new GoToFileV1()));
                }
                else if (lowercaseArg == "/view")
                {
                    if (args.Length < 2)
                    {
                        MessageBox.Show("Invalid parameters", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    GoToFileV1 file;
                    try
                    {
                        file = getFile(args[1]);
                    }
                    catch (Exception)
                    {
                        return;
                    }
                    if (file.notes == "" || file.notes == null)
                    {
                        if (file.url == null || file.url == "")
                        {
                            MessageBox.Show(Strings.Strings.Error_NoURL, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                        try
                        {
                            file.Open();
                        }
                        catch (Exception)
                        {
                            MessageBox.Show(Strings.Strings.Error_ViewerInvalidURL, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        Application.Run(new Viewer(file));
                    }
                }
                else if (lowercaseArg == "/viewurl")
                {
                    if (args.Length < 2)
                    {
                        MessageBox.Show("Invalid parameters", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    GoToFileV1 file;
                    try
                    {
                        file = getFile(args[1]);
                    }
                    catch (Exception)
                    {
                        return;
                    }
                    if (file.url == null || file.url == "")
                    {
                        MessageBox.Show(Strings.Strings.Error_NoURL, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    try
                    {
                        file.Open();
                    }
                    catch (Exception)
                    {
                        MessageBox.Show(Strings.Strings.Error_ViewerInvalidURL, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (lowercaseArg == "/viewnotes")
                {
                    if (args.Length < 2)
                    {
                        MessageBox.Show("Invalid parameters", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    GoToFileV1 file;
                    try
                    {
                        file = getFile(args[1]);
                    }
                    catch (Exception)
                    {
                        return;
                    }
                    Application.Run(new Viewer(file));
                }
                else
                {
                    GoToFileV1 file;
                    try
                    {
                        file = getFile(args[0]);
                    }
                    catch (Exception)
                    {
                        return;
                    }
                    Application.Run(new Editor(file));
                }
            }
        }
    }
}
