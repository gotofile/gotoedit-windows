﻿namespace GoToEditor
{
    partial class Viewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Viewer));
            this.notesTextBox = new System.Windows.Forms.TextBox();
            this.textBoxContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.copyContextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectAllContextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notesLabel = new System.Windows.Forms.Label();
            this.urlOpenLabel = new System.Windows.Forms.LinkLabel();
            this.linkContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.copyURLContextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editLinkLabel = new System.Windows.Forms.LinkLabel();
            this.textBoxContextMenuStrip.SuspendLayout();
            this.linkContextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // notesTextBox
            // 
            resources.ApplyResources(this.notesTextBox, "notesTextBox");
            this.notesTextBox.ContextMenuStrip = this.textBoxContextMenuStrip;
            this.notesTextBox.Name = "notesTextBox";
            this.notesTextBox.ReadOnly = true;
            // 
            // textBoxContextMenuStrip
            // 
            this.textBoxContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyContextToolStripMenuItem,
            this.selectAllContextToolStripMenuItem});
            this.textBoxContextMenuStrip.Name = "textBoxContextMenuStrip";
            resources.ApplyResources(this.textBoxContextMenuStrip, "textBoxContextMenuStrip");
            // 
            // copyContextToolStripMenuItem
            // 
            this.copyContextToolStripMenuItem.Name = "copyContextToolStripMenuItem";
            resources.ApplyResources(this.copyContextToolStripMenuItem, "copyContextToolStripMenuItem");
            this.copyContextToolStripMenuItem.Click += new System.EventHandler(this.copyContextToolStripMenuItem_Click);
            // 
            // selectAllContextToolStripMenuItem
            // 
            this.selectAllContextToolStripMenuItem.Name = "selectAllContextToolStripMenuItem";
            resources.ApplyResources(this.selectAllContextToolStripMenuItem, "selectAllContextToolStripMenuItem");
            this.selectAllContextToolStripMenuItem.Click += new System.EventHandler(this.selectAllContextToolStripMenuItem_Click);
            // 
            // notesLabel
            // 
            resources.ApplyResources(this.notesLabel, "notesLabel");
            this.notesLabel.Name = "notesLabel";
            // 
            // urlOpenLabel
            // 
            resources.ApplyResources(this.urlOpenLabel, "urlOpenLabel");
            this.urlOpenLabel.ContextMenuStrip = this.linkContextMenuStrip;
            this.urlOpenLabel.Name = "urlOpenLabel";
            this.urlOpenLabel.TabStop = true;
            this.urlOpenLabel.Click += new System.EventHandler(this.urlOpenLabel_Click);
            // 
            // linkContextMenuStrip
            // 
            this.linkContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyURLContextToolStripMenuItem});
            this.linkContextMenuStrip.Name = "linkContextMenuStrip";
            resources.ApplyResources(this.linkContextMenuStrip, "linkContextMenuStrip");
            // 
            // copyURLContextToolStripMenuItem
            // 
            this.copyURLContextToolStripMenuItem.Name = "copyURLContextToolStripMenuItem";
            resources.ApplyResources(this.copyURLContextToolStripMenuItem, "copyURLContextToolStripMenuItem");
            this.copyURLContextToolStripMenuItem.Click += new System.EventHandler(this.copyURLContextToolStripMenuItem_Click);
            // 
            // editLinkLabel
            // 
            resources.ApplyResources(this.editLinkLabel, "editLinkLabel");
            this.editLinkLabel.Name = "editLinkLabel";
            this.editLinkLabel.TabStop = true;
            this.editLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.editLinkLabel_LinkClicked);
            // 
            // Viewer
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.editLinkLabel);
            this.Controls.Add(this.notesTextBox);
            this.Controls.Add(this.notesLabel);
            this.Controls.Add(this.urlOpenLabel);
            this.Name = "Viewer";
            this.Icon = global::GoToEditor.Properties.Resources.Icon;
            this.textBoxContextMenuStrip.ResumeLayout(false);
            this.linkContextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox notesTextBox;
        private System.Windows.Forms.Label notesLabel;
        private System.Windows.Forms.LinkLabel urlOpenLabel;
        private System.Windows.Forms.LinkLabel editLinkLabel;
        private System.Windows.Forms.ContextMenuStrip textBoxContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem copyContextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectAllContextToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip linkContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem copyURLContextToolStripMenuItem;
    }
}