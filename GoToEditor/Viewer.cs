﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GoToEditor
{
    public partial class Viewer : Form
    {
        GoToFileV1 file;

        public Viewer(GoToFileV1 file)
        {
            this.file = file;
            InitializeComponent();
            if (file.FilePath == null)
            {
                Text = Strings.Strings.UntitledFilename;
            }
            else
            {
                Text = file.FilePath.Replace("\\\\", "\\");
            }
            Text += " - " + Application.ProductName;

            if (file.notes == null)
            {
                notesTextBox.Text = "";
            }
            else
            {
                notesTextBox.Text = file.notes.Replace("\n", Environment.NewLine);
            }
        }

        private void safeCopy(string text)
        {
            // this function exists because Clipboard.SetText throws an exception if text == ""
            // this function therefore does not attempt to modify the clipboard if text == null || text == ""
            if (text != null && text != "")
            {
                Clipboard.SetText(text);
            }
        }

        private void urlOpenLabel_Click(object sender, EventArgs e)
        {
            if (file.url == null || file.url == "")
            {
                MessageBox.Show(this, Strings.Strings.Error_NoURL, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                file.Open();
            }
            catch (Exception)
            {
                MessageBox.Show(this, Strings.Strings.Error_ViewerInvalidURL, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void aboutLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            new AboutBox().ShowDialog(this);
        }

        private void editLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (file.FilePath == null)
            {
                MessageBox.Show(this, Strings.Strings.Error_EditForbidden, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                Process.Start(Application.ExecutablePath, "\"" + file.FilePath + "\"");
                Close();
            }
        }

        private void copyContextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (notesTextBox.SelectionLength == 0)
            {
                safeCopy(notesTextBox.Text);
            }
            else
            {
                safeCopy(notesTextBox.SelectedText);
            }
        }

        private void selectAllContextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            notesTextBox.SelectionStart = 0;
            notesTextBox.SelectionLength = notesTextBox.Text.Length;
        }

        private void copyURLContextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            safeCopy(file.url);
        }
    }
}
