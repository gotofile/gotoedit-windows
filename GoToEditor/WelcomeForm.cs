﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GoToEditor
{
    public partial class WelcomeForm : Form
    {
        public WelcomeForm()
        {
            InitializeComponent();
        }

        private void newButton_Click(object sender, EventArgs e)
        {
            Process.Start(Application.ExecutablePath, "/new");
        }

        private void openButton_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                Process.Start(Application.ExecutablePath, "\"" + openFileDialog.FileName.Replace("\"", "\"\"").Replace("\\", "\\\\") + "\"");
            }
        }

        private void aboutButton_Click(object sender, EventArgs e)
        {
            new AboutBox().ShowDialog(this);
        }

        private void WelcomeForm_Load(object sender, EventArgs e)
        {
            Text = Application.ProductName;
        }
    }
}
