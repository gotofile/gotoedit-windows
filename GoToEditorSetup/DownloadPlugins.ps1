# This script downloads the NSIS plugins required by this installer.


$scriptdir = Split-Path -parent $PSCommandPath

$webclient = New-Object System.Net.WebClient

# DotNetChecker plugin
Write-Output "Downloading DotNetChecker"
$dotNetCheckerPlugin = "https://codeload.github.com/ReVolly/NsisDotNetChecker/zip/master"
$dotNetCheckerDestinationFile = "$($scriptdir)\NsisDotNetChecker-master.zip"
$dotNetCheckerOriginalDir = "$($scriptdir)\NsisDotNetChecker-master"
$dotNetCheckerDestinationDir = "$($scriptdir)\NsisDotNetChecker"
Write-Output $dotNetCheckerDestinationFile
$webclient.DownloadFile($dotNetCheckerPlugin, $dotNetCheckerDestinationFile)
Write-Output "Expanding archive"
Expand-Archive $dotNetCheckerDestinationFile -DestinationPath $scriptdir -Force
Remove-Item -Path $dotNetCheckerDestinationFile
Rename-Item -Path $dotNetCheckerOriginalDir -NewName $dotNetCheckerDestinationDir
